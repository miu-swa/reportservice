FROM maven:3.8.4-openjdk-17 AS build

WORKDIR /app

COPY . /app
RUN mvn package
COPY pom.xml ./

COPY src src
RUN mvn package -DskipTests

FROM openjdk:17-alpine
WORKDIR /app

COPY --from=build /app/target/ReportingService-1.0.0.jar /app/reportingService.jar
ENV SPRING_PROFILES_ACTIVE default
#ENV SERVER.PORT 8787
EXPOSE 443
EXPOSE 80
ENTRYPOINT ["java", "-jar", "reportingService.jar"]
